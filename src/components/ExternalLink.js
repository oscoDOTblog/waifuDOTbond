import Link from 'next/link';

export default function ExternalLink({ text, url }) {
  return (
    <Link legacyBehavior href={url}>
      <a target="_blank" rel="noopener noreferrer">
        {text}
      </a> 
    </Link>  
  );
}