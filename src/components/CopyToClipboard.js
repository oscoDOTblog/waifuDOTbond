import React, { useState } from 'react';
import styles from '../styles/copyToClipboard.module.css';
import buttonStyles from '../styles/button.module.css';

const CopyToClipboard = ({ textButtonInitial, textToCopy }) => {
  const [copied, setCopied] = useState(false);

  const copyToClipboard = () => {
    const textField = document.createElement('textarea');
    textField.innerText = textToCopy;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
    setCopied(true);
  };

  return (
    <>
      {/* <p className={copied ? styles['fade-up'] : ''}>{textToCopy}</p> */}
      <p>{textToCopy}</p>
      <button
        className={`${buttonStyles.button} ${copied ? styles.fade : ''}`}
        onClick={copyToClipboard}
      >
        {copied ? 'Copied!' : textButtonInitial}
      </button>
    </>
  );
};

export default CopyToClipboard;
