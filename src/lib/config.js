const DONATE_URL = "https://ko-fi.com/osco"
const MONERO_ADDRESS = "84SJZ827YHzHFg5bA7Y9MkJKyMdazoZFfFFPLriwLnPHcwjc6tJNj3mEa2xmvVT7ckdQxHFAJRwrkF2YriSpQmX2P659tF6"
const SITE_NAME = "waifuDOTbond"
const SITE_TITLE = "waifuDOTbond"
const SOCIALS = [
  // {   
  //   "name": "Chatpad AI", 
  //   "desc": "ChatGPT client where all data is saved locally - no tracking, no cookies, no BS!", 
  //   "link": "https://chatpad-ai.waifu.bond"
  // },
  // {   
  //   "name": "Invidious", 
  //   "desc": "For watching YouTube videos (without ads)", 
  //   "link": "https://invidious.waifu.bond"
  // },
  {   
    "name": "IT Tools", 
    "desc": "Collection of handy online tools for developers, with great UX", 
    "link": "https://tools.waifu.bond"
  },
  // {
  //   "name": "Libreddit",
  //   "desc": "For viewing Reddit posts with images",
  //   "link": "https://libreddit.waifu.bond"
  // },
  {   
    "name": "Moneroblock", 
    "desc": "Trustless block explorer for the Monero payment network", 
    "link": "https://nero.waifu.bond"
  },
  // {   
  //   "name": "Nitter", 
  //   "desc": "For viewing Twitter posts (kinda glitchy cuz Elon is LOCKING DOWN THE BIRD)", 
  //   "link": "https://nitter.waifu.bond"
  // },
  {   
    "name": "PrivateBin", 
    "desc": "A minimalist, open source online pastebin where the server has zero knowledge of pasted data. Data is encrypted/decrypted in the browser using 256 bits AES", 
    "link": "https://pbin.waifu.bond"
  },
  // {
  //   "name": "SearXNG",
  //   "desc": "A free internet metasearch engine which aggregates results from various search services and databases. Users are neither tracked nor profiled",
  //   "link": "https://searxng.waifu.bond/"
  // },
  // {
  //   "name": "SearXNG (Plugin Installation)",
  //   "desc": "Use our SearXNG instance as your primary search engine for Firefox-based browsers via the Mycroft Project plugin installation process",
  //   "link": "https://mycroftproject.com/install.html?id=119911&basename=waifudotbond_searxng&icontype=png&name=SearXNG+%28waifuDOTbond%29+"
  // },
  // {
  //   "name": "Teddit",
  //   "desc": "For viewing Reddit posts with text",
  //   "link": "https://teddit.waifu.bond/"
  // },
  // {
  //   "name": "Whoogle",
  //   "desc": "Get Google search results, but without any ads, JavaScript, AMP links, cookies, or IP address tracking",
  //   "link": "https://whoogle.waifu.bond/"
  // },
]
export {
  DONATE_URL,
  MONERO_ADDRESS,
  SITE_NAME,
  SITE_TITLE,
  SOCIALS
}