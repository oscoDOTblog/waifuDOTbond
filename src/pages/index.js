// NextJS Imports
import Head from 'next/head';
import Link from 'next/link';

// Component Imports
import CopyToClipboard from '../components/CopyToClipboard';
import ExternalLink from '../components/ExternalLink';
import Layout from '../components/layout';

// Library Imports
import { DONATE_URL, MONERO_ADDRESS, SITE_TITLE, SITE_ABOUT, SOCIALS } from '../lib/config';

// Styling Imports
import utilStyles from '../styles/utils.module.css';

export default function Home() {
  return (
    <div>
      <Layout home>
        <Head>
          <title>{SITE_TITLE}</title>
        </Head>

        {/* Intro + Projects */}
        <section className={utilStyles.headingMd}>
          {/* Intro */}
          <center>
            <h4>
              {/* A list of 
              <Link legacyBehavior href={"https://en.wikipedia.org/wiki/Free_and_open-source_software"}>
                <a target="_blank" rel="noopener noreferrer">
                  <b>{" FOSS "}</b>
                </a> 
              </Link>
              applications hosted by 
              <Link legacyBehavior href={"https://osco.blog"}>
                <a target="_blank" rel="noopener noreferrer">
                  <b>{" @oscoDOTblog "}</b>
                </a> 
              </Link>
              with 
              <Link legacyBehavior href={"https://runtipi.io "}>
                <a target="_blank" rel="noopener noreferrer">
                  <b>{" RunTipi "}</b>
                </a> 
              </Link>
              for viewing social media posts without exposing your privacy. */}
              El Psy Kongroo
            </h4>
          </center>
        </section>

        {/* Apps */}
        <section className={utilStyles.headingMd}>
          <h2>Apps</h2>
          {SOCIALS.map(({name, desc, link}) => (
            <p key={name}>
              <Link legacyBehavior href={link}>
                <a target="_blank" rel="noopener noreferrer">
                  {name}
                </a> 
              </Link> - {desc}
              {/* <CopyToClipboard className={copyStyles} text={desc} /> */}
            </p>
          ))}
          {/* <h2>Donations</h2>
          <p>
            If you want to <ExternalLink text={"toss a coin to your witcher, "} url={"https://invidious.waifu.bond/watch?v=U9OQAySv184"}/>
            considering doing so here: 
            <br/>
            <b>
            <ExternalLink text={"Ko-fi (@osco)"} url={"https://ko-fi.com/osco"}/>
            </b>
          </p>
          <p>
          If you live in 3023, you can donate with <ExternalLink text={"Monero"} url={"https://www.getmonero.org/"}/> as well:
          <br/>
          <b>
            <CopyToClipboard 
              textButtonInitial={"Copy Monero Address"}
              textToCopy={MONERO_ADDRESS}
            /> 
          </b>             
          </p>           */}
        </section>
      </Layout>
    </div>
  );
}
